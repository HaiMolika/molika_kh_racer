// import React, { useState } from 'react'
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

// function Test() {

//     const [selectedDate, setSelectDate] = useState(null);

//     return (
//         <div>
//            <DatePicker 
//                 selected={selectedDate}
//                 onChange={date => setSelectDate(date)}
//            /> 
//         </div>
//     )
// }

// export default Test

import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class Test extends Component {

    constructor(){
        super();
        this.state = {
            date : null
        };
    }

    dateOnChange = (evt) => {
        this.setState({date : evt});
    }

    render() {
        return (
            <div>
                <DatePicker 
                    className= 'rounded-pill'
                    selected={this.state.date}
                    onChange={this.dateOnChange}
                    placeholderText='ថ្ងៃ ខែ​ ឆ្នាំកំណើត'
                    dateFormat='dd/MM/yyyy'
                    
                /> 
            </div>
        )
    }
}

