import React from 'react';
import Sign_in from './components/signin_form/index'
import 'bootstrap/dist/css/bootstrap.min.css';
import Sign_up from './components/signup_form/index';
import Test from './components/Test';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/login'component={Sign_in}/>
        <Route exact path='/signup' component={Sign_up}/>
        <Route exact path='/test' component={Test}/>
      </Switch>
    </Router>
  );
}

export default App;
